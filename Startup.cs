using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeminioApi.Config;
using GeminioApi.Data;
using GeminioApi.Exceptions;
using GeminioApi.Middleware;
using GeminioApi.Response;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Microsoft.EntityFrameworkCore;

namespace GeminioApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "GeminioApi", Version = "v1" });
            });

            services.AddDbContext<UploadedFileContext>(options =>
                options.UseMySql(Configuration.GetConnectionString("Default"),
                    ServerVersion.AutoDetect(Configuration.GetConnectionString("Default")))
            );

            services.AddOptions();
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "GeminioApi v1"));
                app.UseCors(
                    options => options.WithOrigins("*").AllowAnyMethod().AllowAnyHeader()
                );
            }

            // app.UseHttpsRedirection();

            app.UseExceptionHandler(c => c.Run(async context =>
            {
                var exception = context.Features
                    .Get<IExceptionHandlerPathFeature>()
                    .Error;

                if (exception is HttpStatusException statusException)
                {
                    context.Response.StatusCode = (int) statusException.StatusCode;
                }
                
                var response = new ErrorResponse(exception.Message);
                await context.Response.WriteAsJsonAsync(response);
            }));

            app.UseRouting();

            app.UseMiddleware<JwtMiddleware>();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

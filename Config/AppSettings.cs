namespace GeminioApi.Config
{
    public class AppSettings
    {
        public string FilesBasePath { get; set; }
        public string JwtPublicKey { get; set; }
        public string DomainKey { get; set; }
    }
}
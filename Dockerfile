# This image assumes the app is already build (due to https://github.com/dotnet/dotnet-docker/issues/1291#issuecomment-527535851)
FROM mcr.microsoft.com/dotnet/aspnet:5.0-buster-slim

WORKDIR /app

ENV ASPNETCORE_URLS=http://+:5000

COPY app/ .

RUN groupadd -r dotnet && useradd -g dotnet dotnetuser
USER dotnetuser

EXPOSE 5000

ENTRYPOINT ["dotnet", "GeminioApi.dll"]
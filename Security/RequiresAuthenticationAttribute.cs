using System;
using System.Net;
using GeminioApi.Exceptions;
using Microsoft.AspNetCore.Mvc.Filters;

namespace GeminioApi.Security
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class RequiresAuthenticationAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var tokenStatusObj = context.HttpContext.Items["TokenStatus"];
            if (tokenStatusObj is not TokenStatus tokenStatus)
            {
                throw InternalServerError("Unknown token status");
            }

            switch (tokenStatus)
            {
                case TokenStatus.NotPresent:
                    throw Unauthorized("Missing token");
                case TokenStatus.Invalid:
                    throw Unauthorized("Invalid token");
                case TokenStatus.NoDomainAccess:
                    throw Forbidden("You don't have access to this domain");
                case TokenStatus.Valid:
                    break;
                default:
                    throw InternalServerError("Unknown token status");
            }
        }

        public static HttpStatusException Unauthorized(string message)
        {
            return new(HttpStatusCode.Unauthorized, message);
        }

        public static HttpStatusException Forbidden(string message)
        {
            return new(HttpStatusCode.Forbidden, message);
        }

        public static HttpStatusException InternalServerError(string message)
        {
            return new(HttpStatusCode.InternalServerError, message);
        }
    }
}
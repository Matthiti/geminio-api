namespace GeminioApi.Security
{
    public enum TokenStatus
    {
        NotPresent,
        Invalid,
        NoDomainAccess,
        Valid
    }
}
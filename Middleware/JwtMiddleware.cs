using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using GeminioApi.Config;
using GeminioApi.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace GeminioApi.Middleware
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly AppSettings _appSettings;

        public JwtMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings)
        {
            _next = next;
            _appSettings = appSettings.Value;
        }

        public async Task Invoke(HttpContext context)
        {
            var authHeader = context.Request.Headers["X-Authorization"].FirstOrDefault();
            if (authHeader == null || !authHeader.StartsWith("Bearer "))
            {
                await Continue(context, TokenStatus.NotPresent);
                return;
            }

            var token = authHeader["Bearer ".Length..];
            SecurityToken validatedToken;
            try
            {
                var key = RSA.Create();
                key.ImportFromPem(_appSettings.JwtPublicKey);

                var tokenHandler = new JwtSecurityTokenHandler();
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new RsaSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                }, out validatedToken);
            }
            catch
            {
                await Continue(context, TokenStatus.Invalid);
                return;
            }

            var jwtToken = (JwtSecurityToken) validatedToken;

            var domains = jwtToken.Claims.Where(x => x.Type == "domains");
            if (domains.All(x => x.Value != _appSettings.DomainKey))
            {
                await Continue(context, TokenStatus.NoDomainAccess);
                return;
            }
            
            var userUuid = jwtToken.Claims.First(x => x.Type == "uuid").Value;
            context.Items["UserUuid"] = userUuid;
            await Continue(context, TokenStatus.Valid);
        }

        private async Task Continue(HttpContext context, TokenStatus tokenStatus)
        {
            context.Items["TokenStatus"] = tokenStatus;
            await _next(context);
        }
    }
}
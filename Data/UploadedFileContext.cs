using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using GeminioApi.Models;

namespace GeminioApi.Data
{
    public class UploadedFileContext : DbContext
    {
        public UploadedFileContext(DbContextOptions<UploadedFileContext> options)
            : base(options)
        {
        }

        public DbSet<GeminioApi.Models.UploadedFile> UploadedFile { get; set; }
    }
}
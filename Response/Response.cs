namespace GeminioApi.Response
{
    public abstract class Response
    {
        public bool Success { get; set; }

        public Response(bool success)
        {
            Success = success;
        }
    }
}
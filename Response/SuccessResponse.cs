namespace GeminioApi.Response
{
    public class SuccessResponse<T> : Response
    {
        public T Data { get; set; }

        public SuccessResponse(T data) : base(true)
        {
            Data = data;
        }
    }
}
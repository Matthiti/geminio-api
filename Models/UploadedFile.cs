using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace GeminioApi.Models
{
    public class UploadedFile
    {
        [JsonIgnore]
        public int Id { get; set; }
        
        [Required]
        [StringLength(20)]
        public string Slug { get; set; }
        
        [Required]
        [StringLength(100)]
        public string FileName { get; set; }
        
        [Required]
        [StringLength(254)]
        [JsonIgnore]
        public string FilePath { get; set; }
        
        [Required]
        [StringLength(36)]
        public string Owner { get; set; }
        
        public long UploadedAt { get; set; }
        
        public long? AvailableUntil { get; set; }
    }
}
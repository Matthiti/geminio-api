using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using GeminioApi.Config;
using GeminioApi.Data;
using GeminioApi.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using GeminioApi.Models;
using GeminioApi.Response;
using GeminioApi.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace GeminioApi.Controllers
{
    [Route("api/files")]
    [ApiController]
    public class UploadedFileController : ControllerBase
    {
        private readonly UploadedFileContext _context;
        private readonly AppSettings _appSettings;

        public UploadedFileController(UploadedFileContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }

        [HttpGet("{slug}")]
        public async Task<ActionResult<SuccessResponse<UploadedFile>>> GetFile(string slug)
        {
            var uploadedFile = await _context.UploadedFile.SingleOrDefaultAsync(f => f.Slug == slug);
            if (uploadedFile == null)
            {
                throw new HttpStatusException(HttpStatusCode.NotFound, "File not found");
            }

            return new SuccessResponse<UploadedFile>(uploadedFile);
        }

        [HttpGet("{slug}/download")]
        public async Task<IActionResult> DownloadFile(string slug)
        {
            var uploadedFile = await _context.UploadedFile.SingleOrDefaultAsync(f => f.Slug == slug);
            if (uploadedFile == null)
            {
                throw new HttpStatusException(HttpStatusCode.NotFound, "File not found");
            }

            var stream = new FileStream(uploadedFile.FilePath, FileMode.Open, FileAccess.Read);
            return new FileStreamResult(stream, "application/octet-stream")
            {
                FileDownloadName = uploadedFile.FileName
            };
        }

        [RequiresAuthentication]
        [HttpDelete("{slug}")]
        public async Task<IActionResult> DeleteFile(string slug)
        {
            var owner = (string) HttpContext.Items["UserUuid"];
            var uploadedFile = await _context.UploadedFile.SingleOrDefaultAsync(f => f.Slug == slug && f.Owner == owner);
            if (uploadedFile == null)
            {
                throw new HttpStatusException(HttpStatusCode.NotFound, "File not found");
            }
            
            System.IO.File.Delete(uploadedFile.FilePath);

            _context.UploadedFile.Remove(uploadedFile);
            await _context.SaveChangesAsync();
            
            return NoContent();
        }

        // https://stackoverflow.com/a/54932877
        [RequiresAuthentication]
        [HttpPost("upload")]
        [RequestSizeLimit(1_000_000_000)]
        [RequestFormLimits(MultipartBodyLengthLimit = 1_000_000_000)]
        public async Task<ActionResult<SuccessResponse<UploadedFile>>> UploadFile([FromForm] IFormFile file, [FromForm] string json)
        {
            if (file == null)
            {
                throw new HttpStatusException(HttpStatusCode.BadRequest, "File missing");
            }
            
            var owner = (string) HttpContext.Items["UserUuid"];

            var basePath = Path.Combine(_appSettings.FilesBasePath, owner!);
            if (!Directory.Exists(basePath))
            {
                Directory.CreateDirectory(basePath);
            }

            var filePath = Path.Combine(basePath, Guid.NewGuid().ToString());

            await using (Stream fileStream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }

            UploadedFile uploadedFile = new UploadedFile
            {
                Slug = RandomString(10),
                FileName = file.FileName,
                FilePath = filePath,
                Owner = owner,
                UploadedAt = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds()
            };

            _context.UploadedFile.Add(uploadedFile);
            await _context.SaveChangesAsync();
            
            return CreatedAtAction("GetFile", new { slug = uploadedFile.Slug }, new SuccessResponse<UploadedFile>(uploadedFile));
        }

        private static string RandomString(int lenght)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, lenght)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}

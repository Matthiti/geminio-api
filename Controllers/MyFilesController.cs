using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GeminioApi.Data;
using GeminioApi.Models;
using GeminioApi.Response;
using GeminioApi.Security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GeminioApi.Controllers
{
    [Route("api/my-files")]
    [ApiController]
    public class MyFilesController : ControllerBase
    {
        private readonly UploadedFileContext _context;

        public MyFilesController(UploadedFileContext context)
        {
            _context = context;
        }

        [RequiresAuthentication]
        [HttpGet]
        public async Task<ActionResult<SuccessResponse<IList<UploadedFile>>>> GetMyFiles()
        {
            var owner = (string) HttpContext.Items["UserUuid"];
            return new SuccessResponse<IList<UploadedFile>>(
                await _context.UploadedFile.Where(f => f.Owner == owner).ToListAsync()
            );
        }
    }
}
# Geminio API

## Run

```
dotnet run
```

## Scaffold controller

```
dotnet aspnet-codegenerator controller -name <controller> -async -api -m <model> -dc <context> -outDir Controllers
```

## Create migration

```
dotnet ef migrations add <name>
```

## Run migrations

```
dotnet ef database update
```

## Generate migration script

```
dotnet ef migrations script --output prod.sql
```